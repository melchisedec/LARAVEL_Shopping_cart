<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new \App\Product([
        	'imagePath' => 'http://localhost/PROJECTS/web/ci/ecommerce/a/4/big_pics/Acer_2_in_1.jpg', 
        	'title' => 'Acer spin 3', 
        	'description' => "Flip, rotate, or detach in a snap -- the Aspire Switch 10's unique Acer Snap Hinge uses the invisible force of magnetism to provide a latch-less docking solution that lets you smoothly remove and re-attach the keyboard to the display.", 
        	'price' => '458'
        ]);
        $product->save();

        $product = new \App\Product([
        	'imagePath' => 'http://localhost/PROJECTS/web/images/ACER/acerspin3.jpg', 
        	'title' => 'Acer 2 in 1', 
        	'description' => "The new-generation Corning® Gorilla® Glass 3 makes your display tougher than ever with Native Damage Resistance. Corning® Gorilla® Glass 3 provides 3x better scratch resistance and reduces existing scratch visibility..", 
        	'price' => '690'
        ]);
        $product->save();

        $product = new \App\Product([
        	'imagePath' => 'http://localhost/PROJECTS/web/ci/ecommerce/a/4/big_pics/Acer_Aspire_E15.jpg', 
        	'title' => 'Acer Aspire E15', 
        	'description' => "The Aspire E 15 is powered by an Intel® Atom™ CPU and transforms into four distinct modes that enable you to multitask in any environment. Detach and use it as a tablet, get tasks done in laptop mode, pivot its screen to share, or flip to watch a movie when space is limited..", 
        	'price' => '700'
        ]);
        $product->save();

        $product = new \App\Product([
        	'imagePath' => 'http://localhost/PROJECTS/web/ci/ecommerce/a/4/big_pics/Acer_Aspire_ES1.jpg', 
        	'title' => 'Acer Aspire ES1', 
        	'description' => "Acer Aspire ES1's innovative nano-imprint technology (NIL) offers a wide range of stylish laptop cover options using new environmentally friendly manufacturing processes. The rear cover of the Aspire Switch 10 is made from aluminum and NIL technology and is finished with a gentle gray brush stroke, creating an elegant and unique design.", 
        	'price' => '458'
        ]);
        $product->save();

        $product = new \App\Product([
        	'imagePath' => 'http://localhost/PROJECTS/web/ci/ecommerce/a/4/big_pics/Acer_Aspire_S7-392-6411.jpg', 
        	'title' => 'Acer Aspire S7', 
        	'description' => "At a mere 8.9 mm thin in tablet mode, the Aspire Switch 10 transforms into one of the slimmest 2-in-1 laptops you can buy. It weighs in at just 1.17 kg, so you can effortlessly keep the Aspire Switch 10 at your side all day long.", 
        	'price' => '458'
        ]);
        $product->save();

        $product = new \App\Product([
        	'imagePath' => 'http://localhost/PROJECTS/web/ci/ecommerce/a/4/big_pics/Acer_R7.jpg', 
        	'title' => 'Acer R7', 
        	'description' => "Acer R7's Zero Air Gap technology unites touch and sight better than ever for a truly impressive touchscreen experience. This technology directly bonds the module to the touch panel thereby eliminating reflections, which can degrade contrast and viewing angles, especially in sunlight conditions.", 
        	'price' => '566'
        ]);
        $product->save();

        $product = new \App\Product([
        	'imagePath' => 'http://localhost/PROJECTS/web/ci/ecommerce/a/4/big_pics/Acer_swift_7.jpg', 
        	'title' => 'Acer swift 7', 
        	'description' => "Whether you're an indoors person, an outdoors person, or a bit of both, the Aspire Switch 10 is designed to be used every day, everywhere you go. Its Acer LumiFlex™ display technology provides optimized color control that lets you watch videos and share photos even under the sun.", 
        	'price' => '600'
        ]);
        $product->save();

        $product = new \App\Product([
        	'imagePath' => 'http://localhost/PROJECTS/web/ci/ecommerce/a/4/big_pics/acer_one_tablet_hybrid.jpg', 
        	'title' => 'Acer one Tablet Hybrid', 
        	'description' => "Acer one Tablet Hybrid's Gap technology unites touch and sight better than ever for a truly impressive touchscreen experience. This technology directly bonds the module to the touch panel thereby eliminating reflections, which can degrade contrast and viewing angles, especially in sunlight conditions.", 
        	'price' => '860'
        ]);
        $product->save();

        $product = new \App\Product([
        	'imagePath' => 'http://localhost/PROJECTS/web/ci/ecommerce/a/4/big_pics/Aceraspirer_R13.jpg', 
        	'title' => 'Acer aspire R13', 
        	'description' => "Whether you're an indoors person, an outdoors person, or a bit of both, the Aspire Switch 10 is designed to be used every day, everywhere you go. Its Acer LumiFlex™ display technology provides optimized color control that lets you watch videos and share photos even under the sun.", 
        	'price' => '750'
        ]);
        $product->save();

        $product = new \App\Product([
        	'imagePath' => 'http://localhost/PROJECTS/web/ci/ecommerce/a/4/big_pics/Acer-aspire-s7-191-6640.jpg', 
        	'title' => 'Acer aspire s7 191', 
        	'description' => "At a mere 8.9 mm thin in tablet mode, the Aspire Switch 10 transforms into one of the slimmest 2-in-1 laptops you can buy. It weighs in at just 1.17 kg, so you can effortlessly keep the Aspire Switch 10 at your side all day long..", 
        	'price' => '789'
        ]);
        $product->save();

        $product = new \App\Product([
        	'imagePath' => 'http://localhost/PROJECTS/web/ci/ecommerce/a/4/big_pics/acer-aspire-vx-15-01.jpg', 
        	'title' => 'acer aspire vx', 
        	'description' => "Acer's innovative nano-imprint technology (NIL) offers a wide range of stylish laptop cover options using new environmentally friendly manufacturing processes. The rear cover of the Aspire Switch 10 is made from aluminum and NIL technology and is finished with a gentle gray brush stroke, creating an elegant and unique design..", 
        	'price' => '260'
        ]);
        $product->save();

        $product = new \App\Product([
        	'imagePath' => 'http://localhost/PROJECTS/web/images/ACER/acerspin3.jpg', 
        	'title' => 'Acer spin 3', 
        	'description' => "Flip, rotate, or detach in a snap -- the Aspire Switch 10's unique Acer Snap Hinge uses the invisible force of magnetism to provide a latch-less docking solution that lets you smoothly remove and re-attach the keyboard to the display.", 
        	'price' => '458'
        ]);
        $product->save();

        $product = new \App\Product([
        	'imagePath' => 'http://localhost/PROJECTS/web/ci/ecommerce/a/4/big_pics/acer-swift-7.jpg', 
        	'title' => 'Acer swift 7x', 
        	'description' => "Acer's innovative nano-imprint technology (NIL) offers a wide range of stylish laptop cover options using new environmentally friendly manufacturing processes. The rear cover of the Aspire Switch 10 is made from aluminum and NIL technology and is finished with a gentle gray brush stroke, creating an elegant and unique design.", 
        	'price' => '400'
        ]);
        $product->save();

        $product = new \App\Product([
        	'imagePath' => 'http://localhost/PROJECTS/web/ci/ecommerce/a/4/big_pics/Acer_2_in_1.jpg', 
        	'title' => 'Acer 2 in 1', 
        	'description' => "The new-generation Corning® Gorilla® Glass 3 makes your display tougher than ever with Native Damage Resistance. Corning® Gorilla® Glass 3 provides 3x better scratch resistance and reduces existing scratch visibility.", 
        	'price' => '430'
        ]);
        $product->save();
    }
}
