<!DOCTYPE html>
<html>
<head>
	<title>@yield( 'title' )</title>

	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
	<link rel="stylesheet" href="{{asset('css/pricing.css')}}"/>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ URL::to('css/style.css') }}"/>
 
	@yield( 'styles' )

</head>
<body>

@include( 'partials.header' )

<div class="container">
@yield( 'content' )
</div>

<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

@yield( 'scripts' )

</body>
</html>